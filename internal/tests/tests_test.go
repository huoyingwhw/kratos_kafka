package tests

import (
	"encoding/hex"
	"fmt"
	"testing"
	"time"
)

// 16进制转string
func TestT1(t *testing.T) {

	numStr := "2BFF81030101074D65737361676501FF8200010201074865616465727301FF84000104426F6479011000000017FF83040101074865616465727301FF8400010C010C000029FF820101067573657249640575736572310106737472696E670C0D000B68656C6C6F2D757365723100"

	bytes, err := hex.DecodeString(numStr)
	if err != nil {
		panic(err)
	}

	fmt.Println("转换后的字符串: ", string(bytes))

}

func t2() {
	numStr := "2BFF81030101074D65737361676501FF8200010201074865616465727301FF84000104426F6479011000000017FF83040101074865616465727301FF8400010C010C000029FF820101067573657249640575736572310106737472696E670C0D000B68656C6C6F2D757365723100"

	bytes, err := hex.DecodeString(numStr)
	if err != nil {
		panic(err)
	}

	fmt.Println("转换后的字符串: ", string(bytes))
}

func TestT2(t *testing.T) {

	go func() {
		for true {
			t2()
			time.Sleep(time.Second)
		}
	}()

	time.Sleep(time.Second * 6)
}
