package main

import (
	"fmt"
	"github.com/IBM/sarama"
)

func main() {
	// 设置Kafka broker地址
	brokerList := []string{"localhost:9092"}

	// 配置生产者
	config := sarama.NewConfig()
	config.Producer.Return.Successes = true

	// 创建生产者
	producer, err := sarama.NewSyncProducer(brokerList, config)
	if err != nil {
		panic(err)
	}
	defer producer.Close()

	// 发送消息
	topic := "test-topic"
	message := "Hello, Kafka!"

	// 构建消息
	msg := &sarama.ProducerMessage{
		Topic: topic,
		Value: sarama.StringEncoder(message),
	}

	// 发送消息
	partition, offset, err := producer.SendMessage(msg)
	if err != nil {
		fmt.Println("Failed to send message:", err)
	} else {
		fmt.Printf("Message sent to partition %d at offset %d\n", partition, offset)
	}
}
