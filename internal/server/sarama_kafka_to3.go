package server

import (
	"context"
	"fmt"
	"github.com/IBM/sarama"
	"kratos_kafka/internal/conf"
	"kratos_kafka/internal/service"
	"os"
	"os/signal"
	"time"
)

type SaramaTo3 struct {
	// 将GreeterService放进去方便业务处理～
	greeterService *service.GreeterService
}

func NewSaramaTo3Consumer(greeterService *service.GreeterService) *SaramaTo3 {
	return &SaramaTo3{
		greeterService: greeterService,
	}
}

// 详细使用参考：https://juejin.cn/post/6999263126713696293#heading-4
func (s *SaramaTo3) ConsumeClaim(session sarama.ConsumerGroupSession, claim sarama.ConsumerGroupClaim) error {
	// 轮询消息...
	for msg := range claim.Messages() {
		fmt.Printf("从article_to3收到了消息消息>>>: partition: %v, offset: %v, value: %v, mgsTimestamp: %v, \n", msg.Partition, msg.Offset, string(msg.Value), msg.Timestamp)

		// TODO 需要业务处理的话 先json解析一下.....

		// Notice MarkMessage！
		session.MarkMessage(msg, "")

	}

	return nil
}

func (s *SaramaTo3) Setup(sarama.ConsumerGroupSession) error {
	return nil
}

func (s *SaramaTo3) Cleanup(sarama.ConsumerGroupSession) error {
	return nil
}

// 消费 article_to3中的消息
func NewSaramaKafkaArticleTo3ConsumerServer(bootConf *conf.Bootstrap, greeterService *service.GreeterService) *sarama.ConsumerGroup {

	ctx, cancel := context.WithCancel(context.Background())

	// Notice 如何使用TLS连接kafka，参考 tests/sarama_kafka_tls 中的代码～

	// NewConfig的时候初始化了各种配置～
	saramaTo3ConsumerConfig := sarama.NewConfig()
	// Notice 消费者配置 用源码包一定要看一下它的配置
	saramaTo3ConsumerConfig.Consumer.Return.Errors = true // 改成true
	// 未找到组消费位移的时候从哪边开始消费 TODO 默认是 OffsetNewest 从最新的偏移量开始消费
	saramaTo3ConsumerConfig.Consumer.Offsets.Initial = sarama.OffsetNewest
	//saramaTo3ConsumerConfig.Consumer.Offsets.Initial = sarama.OffsetOldest # 从最早的偏移量开始消费
	saramaTo3ConsumerConfig.Consumer.Offsets.AutoCommit.Interval = 1 * time.Second
	saramaTo3ConsumerConfig.Consumer.Offsets.Retry.Max = 3

	// Notice Rebalance 相关配置 跟默认的一样
	saramaTo3ConsumerConfig.Consumer.Group.Rebalance.Timeout = 60 * time.Second
	saramaTo3ConsumerConfig.Consumer.Group.Rebalance.Retry.Max = 4
	saramaTo3ConsumerConfig.Consumer.Group.Rebalance.Retry.Backoff = 2 * time.Second
	saramaTo3ConsumerConfig.Consumer.Group.Rebalance.GroupStrategies = []sarama.BalanceStrategy{sarama.NewBalanceStrategyRange()}
	// Notice 换一下它 也不行,分区扩容还是不能消费到新分区的数据
	//saramaTo3ConsumerConfig.Consumer.Group.Rebalance.GroupStrategies = []sarama.BalanceStrategy{sarama.NewBalanceStrategySticky()}
	//saramaTo3ConsumerConfig.Consumer.Group.Rebalance.GroupStrategies = []sarama.BalanceStrategy{sarama.NewBalanceStrategyRoundRobin()}

	// Notice 下面的配置跟默认配置一样!!!
	saramaTo3ConsumerConfig.Consumer.Fetch.Min = 1
	saramaTo3ConsumerConfig.Consumer.Fetch.Default = 1024 * 1024
	saramaTo3ConsumerConfig.Consumer.Retry.Backoff = 2 * time.Second
	saramaTo3ConsumerConfig.Consumer.MaxWaitTime = 250 * time.Millisecond
	saramaTo3ConsumerConfig.Consumer.MaxProcessingTime = 100 * time.Millisecond

	// Notice 自动提交模式下已经能解决丢消息问题,没有特殊需求不需要手动提交
	// 手动提交消息参考下面的文章: https://juejin.cn/post/6999263126713696293#heading-5
	saramaTo3ConsumerConfig.Consumer.Offsets.AutoCommit.Enable = true

	// Notice new consumer group
	saramaTo3ConsumerGroup, errSaramaTo3ConsumerGroup := sarama.NewConsumerGroup(
		// addr
		bootConf.MessageQueue.Kafka.Server.Addr,
		// group
		bootConf.MessageQueue.Kafka.ArticleGroup3,
		saramaTo3ConsumerConfig,
	)
	if errSaramaTo3ConsumerGroup != nil {
		panic(errSaramaTo3ConsumerGroup)
	}

	// 监听信号，用于优雅的停止消费者 Notice 防止 goroutine泄漏！
	signals := make(chan os.Signal, 1)
	signal.Notify(signals, os.Interrupt, os.Kill)

	// 使用自己定义的 RealConsumer 消费消息 重写 ConsumeClaim 方法处理消费的数据
	// 将greeterService当作参数传进去～
	currSaramaTo3Consumer := NewSaramaTo3Consumer(greeterService)

	// 消费数据
	go func() {
		for {
			select {
			case err := <-saramaTo3ConsumerGroup.Errors():
				if err != nil {
					fmt.Println("saramaTo3ConsumerGroup.Errors():>> ", err)
				}
			// TODO ... 最好写一个 clean函数 将取消信号加进去～～
			case <-signals:
				printStopSignal2()

				saramaTo3ConsumerGroup.Close()
				cancel()
				return
			default:
				errConsume := saramaTo3ConsumerGroup.Consume(
					ctx,
					// topic
					[]string{bootConf.MessageQueue.Kafka.ArticleTopic3},
					// Notice 重写了ConsumeClaim方法，业务中主要的处理逻辑写在这个方法里面
					currSaramaTo3Consumer,
				)
				if errConsume != nil {
					fmt.Println("saramaTo3消费消息发生错误！err: ", errConsume)
				}
			}
		}
	}()

	return &saramaTo3ConsumerGroup
}

func printMsgTest2(msg string) {

	fmt.Println("printMsgTest..............", msg)

}

func printStopSignal2() {
	fmt.Println("收到了停止的信号!!! 停止saramaTo3Consumer...")
}
