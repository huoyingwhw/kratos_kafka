package server

import (
	"context"
	"github.com/go-kratos/kratos/v2/encoding"
	"github.com/go-kratos/kratos/v2/log"
	"github.com/tx7do/kratos-transport/transport/kafka"
	"kratos_kafka/internal/conf"
	"kratos_kafka/internal/service"
	"kratos_kafka/internal/utils/mq_kafka"
)

func NewKafkaConsumerServer(msgConf *conf.MessageQueue, greeterService *service.GreeterService) (*kafka.Server, func()) {
	// Notice 没有用到pem～～～
	//certBytes, err := ioutil.ReadFile(c.Kafka.Pem)
	//if err != nil {
	//	panic(fmt.Sprintf("kafka client read cert file failed %v", err))
	//}
	//clientCertPool := x509.NewCertPool()
	//ok := clientCertPool.AppendCertsFromPEM(certBytes)
	//if !ok {
	//	panic("kafka client failed to parse root certificate")
	//}

	ctx := context.Background()
	kafkaSrv := kafka.NewServer(
		kafka.WithAddress(msgConf.Kafka.Server.Addr), // addr是个列表
		kafka.WithCodec(encoding.GetCodec("json")),
		// Notice 没有用到TLS～～～
		//kafka.WithTLSConfig(&tls.Config{
		//	RootCAs:            clientCertPool,
		//	InsecureSkipVerify: true,
		//}),
		//kafka.WithPlainMechanism(c.Kafka.Username, c.Kafka.Password),
		//kafka.WithBrokerOptions(hello_broker.OptionContextWithValue("max.poll.records", 5000)),
	)

	// 注册一个订阅者，从kafka中消费数据
	err := kafkaSrv.RegisterSubscriber(
		ctx,
		msgConf.Kafka.ArticleTopic, // topic
		msgConf.Kafka.ArticleGroup, // group
		false,
		mq_kafka.RegisterArticleHandler(greeterService.HandleReceiveArticleData),
		mq_kafka.ArticleDataCreator,
	)
	if err != nil {
		panic(err)
	}

	// 手动ACK消息
	err = kafkaSrv.RegisterSubscriber(
		ctx,
		msgConf.Kafka.ArticleTopic2, // topic
		msgConf.Kafka.ArticleGroup2, // group
		// Notice 手动ACK消息，将这个参数设置为true
		true,
		mq_kafka.RegisterArticleHandler2(greeterService.HandleReceiveArticleData2),
		mq_kafka.ArticleDataCreator,
	)
	if err != nil {
		panic(err)
	}

	// start
	if err = kafkaSrv.Start(ctx); err != nil {
		panic(err)
	}

	return kafkaSrv, func() {
		if err = kafkaSrv.Stop(ctx); err != nil {
			log.Errorf("kafka expected nil got %v", err)
		}
	}
}
