package biz

import (
	"context"
	"github.com/go-kratos/kratos/v2/errors"
	"github.com/go-kratos/kratos/v2/log"
	"github.com/google/wire"
	v1 "kratos_kafka/api/helloworld/v1"
	"kratos_kafka/internal/conf"
	"kratos_kafka/internal/hello_broker"
)

// ProviderSet is biz providers.
var ProviderSet = wire.NewSet(NewGreeterUsecase)

var (
	// ErrUserNotFound is user not found.
	ErrUserNotFound = errors.NotFound(v1.ErrorReason_USER_NOT_FOUND.String(), "user not found")
)

// Greeter is a Greeter model.
type Greeter struct {
	Hello string
}

// GreeterRepo is a Greater repo.
type GreeterRepo interface {
	Save(context.Context, *Greeter) (*Greeter, error)
}

// GreeterUsecase is a Greeter usecase.
type GreeterUsecase struct {
	repo GreeterRepo
	log  *log.Helper
	// Notice broker
	broker *hello_broker.Broker
	// Notice 项目所有的yaml配置项都放在这里
	yamlConf *conf.Bootstrap
}

// NewGreeterUsecase new a Greeter usecase.
func NewGreeterUsecase(repo GreeterRepo, logger log.Logger, broker *hello_broker.Broker, yamlConf *conf.Bootstrap) *GreeterUsecase {
	return &GreeterUsecase{
		repo:     repo,
		log:      log.NewHelper(logger),
		broker:   broker,
		yamlConf: yamlConf,
	}
}
