package mq_kafka

import "time"

// MQ中数据的结构
type ArticleData struct {
	Title       string    `json:"title"`
	Desc        string    `json:"desc"`
	Tags        []string  `json:"tags"`
	PublishTime time.Time `json:"publish_time"`
}
