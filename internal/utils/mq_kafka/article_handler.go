package mq_kafka

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/tx7do/kratos-transport/broker"
)

// 从kafka中解析ArticleData数据
func ArticleDataCreator() broker.Any { return &ArticleData{} }

// service中处理的函数需要跟它保持一致
type ArticleDataHandler func(_ context.Context, topic string, headers broker.Headers, msg *ArticleData) error

// handle
func RegisterArticleHandler(fnc ArticleDataHandler) broker.Handler {
	return func(ctx context.Context, event broker.Event) error {
		var msg *ArticleData = nil

		eventMsgBody := event.Message().Body
		fmt.Println("eventMsgBody:>>>>> ", eventMsgBody)
		switch t := eventMsgBody.(type) {
		case []byte:
			fmt.Println("byte............................")
			msg = &ArticleData{}
			if err := json.Unmarshal(t, msg); err != nil {
				return err
			}
		case string:
			fmt.Println("string............................")
			msg = &ArticleData{}
			if err := json.Unmarshal([]byte(t), msg); err != nil {
				return err
			}
		case *ArticleData:
			fmt.Println("相同类型............................", t.Title, t.Desc, t.Tags, t.PublishTime)
			msg = t
		default:
			fmt.Println("default............................")
			return fmt.Errorf("unsupported type: %T", t)
		}

		// Notice 使用service中的handler函数做业务处理，把msg传进去～
		fmt.Println("evnet.Message().Headers:>>>>>>>>>>> ", event.Message().Headers)
		if err := fnc(ctx, event.Topic(), event.Message().Headers, msg); err != nil {
			return err
		}

		fmt.Println("msg:>>>>>>>>>>>> ", msg)

		return nil
	}
}

// service中处理的函数需要跟它保持一致
type ArticleDataHandler2 func(_ context.Context, topic string, event broker.Event, headers broker.Headers, msg *ArticleData) error

// handle
func RegisterArticleHandler2(fnc ArticleDataHandler2) broker.Handler {
	return func(ctx context.Context, event broker.Event) error {
		var msg *ArticleData = nil

		eventMsgBody := event.Message().Body
		fmt.Println("eventMsgBody:>>>>> ", eventMsgBody)
		switch t := eventMsgBody.(type) {
		case []byte:
			fmt.Println("byte............................")
			msg = &ArticleData{}
			if err := json.Unmarshal(t, msg); err != nil {
				return err
			}
		case string:
			fmt.Println("string............................")
			msg = &ArticleData{}
			if err := json.Unmarshal([]byte(t), msg); err != nil {
				return err
			}
		case *ArticleData:
			fmt.Println("相同类型............................", t.Title, t.Desc, t.Tags, t.PublishTime)
			msg = t
		default:
			fmt.Println("default............................")
			return fmt.Errorf("unsupported type: %T", t)
		}

		// Notice 使用service中的handler函数做业务处理，把msg传进去～
		fmt.Println("evnet.Message().Headers:>>>>>>>>>>> ", event.Message().Headers)
		if err := fnc(ctx, event.Topic(), event, event.Message().Headers, msg); err != nil {
			return err
		}

		fmt.Println("msg:>>>>>>>>>>>> ", msg)

		return nil
	}
}
