package mq_nsq

import "time"

type BookData struct {
	Title       string    `json:"title"`
	Desc        string    `json:"desc"`
	Authors     []string  `json:"authors"`
	PublishTime time.Time `json:"publish_time"`
}
