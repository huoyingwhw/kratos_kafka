package utils

import (
	"fmt"
	"testing"
	"time"
)

func TestT1(t *testing.T) {

	var ti time.Time
	fmt.Println("ti: ", ti)

	fmt.Println("当前秒级别时间戳: ", Time2TimeStampSecond(GetCurrentTime()))
	fmt.Println("当前毫秒级别时间戳: ", Time2TimeStampMilliSecond(GetCurrentTime()))

}

func TestTimestampSec2Time(t *testing.T) {

	var stampSec int64 = 1681280157
	ret := TimestampSec2Time(stampSec)

	fmt.Printf(">>>>> %T, %v", ret, ret)

}

func TestGetTimeSubSecs(t *testing.T) {
	var stampSec int64 = 1681900577
	t1 := TimestampSec2Time(stampSec)
	fmt.Println("t1: ", t1)
	t2 := GetCurrentTime()
	fmt.Println("t2: ", t2)

	ret1 := GetTimeSubSecs(t1, t2)

	fmt.Printf(">>> ret1Type: %T, ret1: %v, ret1是否大于0: %v \n", ret1, ret1, ret1 > 0)

}

func TestTestAfter7Days(t *testing.T) {

	ret := GetAfter7DaysTimestampSec()
	fmt.Println("ret: ", ret)

	res := fmt.Sprintf(`掉落奖励: %v, <a href="%v">前往查看>></a>`, "a,b,c,d", "#app/backPack")
	fmt.Println("res: ", res)

}

func TestMapRange(t *testing.T) {

	mp := make(map[string]string, 0)
	//var mp map[string]string

	for key := range mp {
		fmt.Println("key: ", key)
	}

}
