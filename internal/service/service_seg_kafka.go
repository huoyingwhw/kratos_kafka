package service

import (
	"context"
	v1 "kratos_kafka/api/helloworld/v1"
)

func (s *GreeterService) SendMsgToKafkaBySegHashPartition(ctx context.Context, in *v1.Empty) (*v1.Empty, error) {
	return s.Uc.SendMsgToKafkaBySegHashPartition(ctx, in)
}
