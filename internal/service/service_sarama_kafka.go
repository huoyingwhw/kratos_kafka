package service

import (
	"context"
	v1 "kratos_kafka/api/helloworld/v1"
)

func (g *GreeterService) SendMsgToKafkaBySaramaPartition(ctx context.Context, req *v1.Empty) (*v1.Empty, error) {
	return g.Uc.SendMsgToKafkaBySaramaPartition(ctx, req)
}
