package service

import (
	"context"
	"github.com/tx7do/kratos-transport/broker"
	v1 "kratos_kafka/api/helloworld/v1"
	"kratos_kafka/internal/utils/mq_nsq"
)

// 往NSQ中发消息
func (s *GreeterService) SendMsgToNSQ(ctx context.Context, req *v1.Empty) (*v1.Empty, error) {
	return s.Uc.SendMsgToNSQ(ctx, req)
}

// 接收NSQ中的数据
func (s *GreeterService) ReceiveMsgFromNSQ(ctx context.Context, event broker.Event, topic string, headers broker.Headers, msg *mq_nsq.BookData) error {
	return s.Uc.ReceiveMsgFromNSQ(ctx, topic, headers, msg)
}
