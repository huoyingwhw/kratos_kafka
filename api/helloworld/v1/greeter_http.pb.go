// Code generated by protoc-gen-go-http. DO NOT EDIT.
// versions:
// - protoc-gen-go-http v2.5.3
// - protoc             v3.21.12
// source: helloworld/v1/greeter.proto

package v1

import (
	context "context"
	http "github.com/go-kratos/kratos/v2/transport/http"
	binding "github.com/go-kratos/kratos/v2/transport/http/binding"
)

// This is a compile-time assertion to ensure that this generated file
// is compatible with the kratos package it is being compiled against.
var _ = new(context.Context)
var _ = binding.EncodeURL

const _ = http.SupportPackageIsVersion1

const OperationGreeterSendMsgToKafka = "/helloworld.v1.Greeter/SendMsgToKafka"

type GreeterHTTPServer interface {
	SendMsgToKafka(context.Context, *SendMsgToKafkaReq) (*Empty, error)
}

func RegisterGreeterHTTPServer(s *http.Server, srv GreeterHTTPServer) {
	r := s.Route("/")
	r.POST("/sendMsgToKafka", _Greeter_SendMsgToKafka0_HTTP_Handler(srv))
}

func _Greeter_SendMsgToKafka0_HTTP_Handler(srv GreeterHTTPServer) func(ctx http.Context) error {
	return func(ctx http.Context) error {
		var in SendMsgToKafkaReq
		if err := ctx.Bind(&in); err != nil {
			return err
		}
		http.SetOperation(ctx, OperationGreeterSendMsgToKafka)
		h := ctx.Middleware(func(ctx context.Context, req interface{}) (interface{}, error) {
			return srv.SendMsgToKafka(ctx, req.(*SendMsgToKafkaReq))
		})
		out, err := h(ctx, &in)
		if err != nil {
			return err
		}
		reply := out.(*Empty)
		return ctx.Result(200, reply)
	}
}

type GreeterHTTPClient interface {
	SendMsgToKafka(ctx context.Context, req *SendMsgToKafkaReq, opts ...http.CallOption) (rsp *Empty, err error)
}

type GreeterHTTPClientImpl struct {
	cc *http.Client
}

func NewGreeterHTTPClient(client *http.Client) GreeterHTTPClient {
	return &GreeterHTTPClientImpl{client}
}

func (c *GreeterHTTPClientImpl) SendMsgToKafka(ctx context.Context, in *SendMsgToKafkaReq, opts ...http.CallOption) (*Empty, error) {
	var out Empty
	pattern := "/sendMsgToKafka"
	path := binding.EncodeURL(pattern, in, false)
	opts = append(opts, http.Operation(OperationGreeterSendMsgToKafka))
	opts = append(opts, http.PathTemplate(pattern))
	err := c.cc.Invoke(ctx, "POST", path, in, &out, opts...)
	if err != nil {
		return nil, err
	}
	return &out, err
}
