//go:build wireinject
// +build wireinject

// The build tag makes sure the stub is not built in the final build.

package main

import (
	"kratos_kafka/internal/biz"
	"kratos_kafka/internal/conf"
	"kratos_kafka/internal/data"
	"kratos_kafka/internal/hello_broker"
	"kratos_kafka/internal/server"
	"kratos_kafka/internal/service"

	"github.com/go-kratos/kratos/v2"
	"github.com/go-kratos/kratos/v2/log"
	"github.com/google/wire"
)

// wireApp init kratos application.
func wireApp(*conf.Server, *conf.Data, *conf.Bootstrap, *conf.MessageQueue, log.Logger) (*kratos.App, func(), error) {
	panic(wire.Build(server.ProviderSet, data.ProviderSet, biz.ProviderSet, service.ProviderSet, hello_broker.ProviderSet, newApp))
}
